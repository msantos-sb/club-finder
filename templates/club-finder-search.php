<?php

/*
Template Name: Club Finder
Template Post Type: clubfinder
*/

get_header();

require plugin_dir_path( __FILE__ ).'/../tests/data.php';
require plugin_dir_path( __FILE__ ).'/../public/options.php';

//has alphabetic chars
if(preg_match("/[a-z]/i", $_GET['postcode_suburb'])){
    $suburb = $_GET['postcode_suburb'];
} else {
    $postcode = $_GET['postcode_suburb'];
}

?>
<div class="wrap club-search">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            while ( have_posts() ) : the_post(); ?>
            
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-md-12 d-none mb-4" id="wrap-map">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-4">
                <div class="row">
                    <div class="col-md-3 t-find-a-club">
                        <h2><a href="/?clubfinder=club-finder">Find a Club</a></h2>
                    </div>
                    <div class="col-md-4 d-flex">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="club-name align-self-center">Cheltenham 3192</div>
                            </div>
                            <div class="col-md-6">
                                <div class="results-number ml-3 align-self-center d-none">(Displaying <span class="r-count">0</span> results)</div>
                            </div>
                        </div>           
                    </div>
                    <div class="col-md-5 d-flex justify-content-end">
                        <form>
                            <div class="input-group wrap-filter">
                                <select class="filter-by form-control" name="filter_by" id="filter_by">
                                    <option value="">Select a filter</option>
                                    <option value="option-function-bookings">has Function Bookings</option>
                                    <option value="option-jack-attack">has Jack Attack</option>
                                    <option value="option-social-bowls">has Social Bowls</option>
                                    <option value="option-meals">has Meals</option>
                                    <option value="option-entertainment">has Entertainment</option>
                                    <option value="option-tab">has Tab</option>
                                </select>
                                <button class="input-group-btn btn btn-secondary" id="clear-filters" type="button">Clear Filters</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="w-form">
                            <form action="" method="GET">
                                <div class="form-group">
                                    <label for="">Postcode</label>
                                    <input type="text" class="form-control"  id="postcode" name="postcode" value="<?php echo !empty($postcode) ? $postcode : 2570; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="long">Suburb</label>
                                    <input id="suburb" type="text" class="form-control"  name="suburb" value="<?php echo !empty($suburb) ? $suburb : "Abbotsbury"; ?>">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="button" id="submit" onclick="ClubFinder.search();">Search for locations</button>
                                </div>
                                <div class="extra-info d-none">
                                    <div class="form-group">
                                        <label for="">Center Latitude</label>
                                        <input type="text" class="form-control"  id="lat" name="lat" value="58.002637" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="long">Center Longitude</label>
                                        <input id="lng" type="text" class="form-control"  name="lng" value="-36.349405" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="long">Distance Radius</label>
                                        <input id="distance_max" type="text" class="form-control"  name="distance_max" value="5000" readonly>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                        </div>   
                    </div>   
                    <div class="col-md-9 w-results">
                        <div id="loading" class="d-none">
                            <div class="d-flex">
                                <img src="<?php echo plugin_dir_url( __FILE__ ); ?>../public/img/loading.gif" class="align-self-center m-auto">
                            </div>
                        </div>
                        <div id="results-start"></div>
                        <ul class="results">                          
                            <li id="result-model" class="d-none">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="r-title">Test</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="distance"><span class="dashicons dashicons-location"></span> <span class="r-distance"></span>km away</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text r-text"></p> 
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="row options">
                                            <li id="option-model" class="col-3 d-none">
                                                <a href="#" class="o-link">
                                                    <figure>
                                                        <img class="img-fluid o-img" title="" alt="" src="">
                                                        <figcaption class="o-title"></figcaption>
                                                    </figure>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <a href="#" class="btn btn-secondary learn-more r-link">Learn more</a>
                                    </div>
                                </div>                       
                                
                            </li>
                        </ul>   
                        
                        <nav aria-label="Page navigation">
                            <ul class="results-pagination pagination justify-content-center d-none">
                                <li class="page-item disabled page-previous">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li id="pagination-model" class="page-item d-none" ><a tabindex="-1" class="page-link" href="#">0</a></li>
                                <li class="page-item page-next">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav> 
                        
                    </div>                 
                </div>
                
            </div>
            
            <div id="json" class="d-none">
                
            </div>
            <script>
                function initMap() {
                    $(document).ready(function(){
                        ClubFinder.init();
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap"></script>
            <script src="<?php echo $_SERVER['REQUEST_SCHEME']; ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/wp-content/plugins/club-finder/public/js/club-finder-public.js"></script>
            <script>
                <?php if($postcode || $suburb): ?>  
                $(document).ready(function(){
                    ClubFinder.search();
                });
                <?php endif; ?>
            </script>
            <?php  
            endwhile; // End of the loop.
            ?>  
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer(); ?>
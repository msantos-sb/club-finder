<?php

/*
Template Name: Club Info
Template Post Type: clubfinder
*/

get_header();
require plugin_dir_path( __FILE__ ).'/../tests/data.php';
require plugin_dir_path( __FILE__ ).'/../public/options.php';

?>

<div class="wrap club-info">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            while ( have_posts() ) : the_post(); ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="breadcrumb">
                            <li>
                                <a href="/?clubfinder=club-finder">Find a Club</a>
                            </li>
                            <li>
                                <a href="/?clubfinder=club-info&club_id=<?php echo get_query_var('club_id'); ?>" class="c-title">Cheltenham 3192</a>
                            </li>
                        </ul>
                    </div>
                    
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">   
                        <div class="club">
                        <div id="result-model" class="result d-none">
                            <div class="row">
                                <div class="col-8">
                                    <p class="title r-title"></p>
                                </div>
                                <div class="col-4 ">
                                    <p class="distance text-right"><span class="dashicons dashicons-location"></span> 
                                    <span class="r-distance"></span> km away</p>
                                </div>
                            </div>
                            <div class="row w-text">
                                <div class="col-12">
                                    <p class="r-text text"></p> 
                                </div>
                            </div>
                            <p class="t-we-offer">We offer</p> 
                            <div class="row">
                                <div class="col-12">
                                    <ul class="row options">
                                        <li id="option-model" class="col-3 d-none">
                                            <a href="#" class="o-link">
                                                <figure>
                                                    <img class="img-fluid o-img" title="" alt="" src="">
                                                    <figcaption class="o-title"></figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>                     
                            
                        </div> 
                    </div> 
                        
                    </div>  
                    <div class="col-md-4">
                        <div class="card contact">
                            <div class="card-body p-0">
                                <!-- $club from data.php -->
                                <p class="title text-uppercase">Contact</p>
                                <ul class="list p-3">
                                    <li>
                                        <span class="w-icon">
                                            <span class="dashicons dashicons-admin-links"></span>
                                        </span>
                                        <span class="t-item c-website">
                                            
                                        </span>
                                    </li>
                                    <li>
                                        <span class="w-icon">
                                            <span class="dashicons dashicons-email"></span>
                                        </span>
                                        <span class="t-item c-email">
                                            
                                        </span>
                                    </li>
                                    <li class="i-phone">
                                        <span class="w-icon">
                                            <span class="dashicons dashicons-phone"></span>
                                        </span>
                                        <span class="t-item c-phone">
                                            
                                        </span>
                                    </li>
                                    <li class="i-available">
                                        <span class="w-icon">
                                            <span class="dashicons dashicons-clock"></span>
                                        </span>
                                        <span class="t-item c-available">
                                            <select>
                                                <option>
                                                    
                                                </option>
                                            </select>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="w-icon">
                                            <span class="dashicons dashicons-location-alt"></span>
                                        </span>
                                        <span class="t-item c-address">
                                            
                                        </span>
                                    </li>
                                </ul>
                                <div id="wrap-map">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>               
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <p class="t-clubs-nearby">Clubs nearby</p>
                        <div id="loading" class="d-none">
                            <div class="d-flex">
                                <img src="<?php echo plugin_dir_url( __FILE__ ); ?>../public/img/loading.gif" class="align-self-center m-auto">
                            </div>
                        </div>
                        <div id="results-start"></div>
                        <ul class="results clubs-nearby nearby options" id="nearby-list">                          
                            
                            <li id="nearby-model" class="result d-none">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="r-title title"><?php echo $r['title'] ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="distance"><span class="dashicons dashicons-location"></span>
                                         <span class="r-distance"></span> km away</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text r-text"></p> 
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="row options justify-content-center">
                                            <li id="option-model" class="col-3 d-none">
                                                <a href="#" class="o-link">
                                                    <figure>
                                                        <img class="img-fluid o-img" title="" alt="" src="">
                                                        <figcaption class="o-title"></figcaption>
                                                    </figure>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <a href="<?php echo $r['link']?>" class="btn btn-secondary learn-more">Learn more</a>
                                    </div>
                                </div>                       
                                
                            </li>
                        </ul> 
                        <nav aria-label="Page navigation">
                            <ul id="nearby-pagination" class="results-pagination pagination justify-content-center d-none">
                              <li class="page-item disabled page-previous">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                              </li>
                              <li id="pagination-model" class="page-item d-none" ><a tabindex="-1" class="page-link" href="#">0</a></li>
                              <li class="page-item page-next">
                                <a class="page-link" href="#">Next</a>
                              </li>
                            </ul>
                        </nav> 
                    </div>
                </div>
                
            </div>
            <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0neLxfRyXGHRpHC0XFqGsUMNoAWHt3gM&amp;callback=initMap" async="" defer=""></script>    -->
            <script>
                function initMap() {
                    $(document).ready(function(){
                        ClubFinder.init();
                        /* show minimap with a single marker */
                        var club_id = $('#club_id').val();
                        ClubFinder.focus(club_id);
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap"></script>
            <script src="<?php echo $_SERVER['REQUEST_SCHEME']; ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/wp-content/plugins/club-finder/public/js/club-finder-public.js"></script>
            <?php  
            endwhile; // End of the loop.
            ?>  
        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer(); ?>
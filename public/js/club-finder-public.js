var map;
var GET_COORDINATES = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
var COUNTRY_ABBR = 'AU';
var HOST_URL = window.location.origin;
/* note that geocode key may differ from maps key */
var GEO_KEY = $('#geo_key').val();
var MAPS_KEY = $('#maps_key').val();
var GET_CLUBS = $('#clubs_api').val();
var GET_NEARBY = $('#clubs_nearby_api').val();
var CLUB_ID = $('#club_id').val();
var DISTANCE_MAX = $('#distance_max').val();
var RESULTS_PER_PAGE = $('#results_per_page').val();

/* 
* title: string
* contact: array ( link, email, phone, available, address)
* options: array ( what we offer)
* nearby: array ( collection of clubs )
*/
var Club = function(data){
	this.id = data.id;
	this.title = data.attributes.clubName;
	this.description = data.attributes.clubDescription;
	this.distance = data.attributes.distance;
	this.contact = data.attributes.clubContact;
	this.options = data.attributes.clubOptions;
	this.nearby = data.attributes.relationships;
	return this;
};
/* 
* Options helpers
*/
var Option = {
	getClass: function(name){
		return 'option-'+name.toLowerCase().replace(/\s+/g, '-');
	},
	getImage: function(name){
		switch(name){
			case "Function Bookings":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			case "Jack Attack":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			case "Social Bowls":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			case "Meals":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			case "Entertainment":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			case "Tab":
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
			default:
			return '/wp-content/plugins/club-finder/public/img/item.png';
			break;
		}
	}
};
var ClubFinder = {
	//init map with coordinates
	init: function(){
		var lat = $('#lat').val();
		var lng = $('#lng').val();
		
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 2,
			center: new google.maps.LatLng(lat,lng),
			mapTypeId: 'terrain',
			streetViewControl: false,
			mapTypeControl: false,
		});  
		
		ClubFinder.getLocation();
	},
	getRequestToken: function(){
		return null;
	},
	//club-info.php need focus in for a single club
	focus: function(club_id){
		//get the club info by its ID
		
		$.ajax({
			method: "GET",
			url: GET_CLUBS,
			dataType : "text",
			data: { token: ClubFinder.getRequestToken(), club_id : club_id}
		})
		.done(function( msg ) {
			var result = JSON.parse(msg);
			
			var club_data = result.data.filter(function(value){
				if(value.id == "CLUB_"+club_id)
				return value;
			});
			
			var club_o = new Club(club_data[0]);
			var marker = ClubFinder.addToMap(club_data[0]);
			map.setCenter(marker.getPosition());
			var row = ClubFinder.displayClub(club_o);
			ClubFinder.fillContact(club_o);
			ClubFinder.fetchNearby(club_o);
			
		}).fail(function(){
			alert('Request Failed');
		});	
		
	},
	//pin a club to the map
	addToMap: function(club){		
		var coords = [club.attributes.clubLatitude, club.attributes.clubLongitude];
		var latLng = new google.maps.LatLng(coords[0],coords[1]);
		
		var infowindow = new google.maps.InfoWindow({
			content: '<div id="content">'+ club.attributes.clubDescription+ '</div>'
		});
		
		var marker = new google.maps.Marker({
			title: club.attributes.clubName+" from "+club.attributes.clubState,
			position: latLng
		});
		
		marker.setMap(map);
		
		marker.addListener('click', function() {
			map.setZoom(8);
			map.setCenter(marker.getPosition());
			infowindow.open(map, marker);
		});
		
		return marker;
		
	},
	//get a single club info and display (club-info.php)
	displayClub: function(club){
		var row = $('#result-model').clone();
		row.removeAttr('id');
		row.find('.r-title').text(club.title);
		row.find('.r-text').html(club.description);
		row.find('.r-distance').html(Math.round(parseInt(club.distance)/100));
		ClubFinder.setOptions(row, club.options);
		row.removeClass('d-none');
		row.addClass('result');
		$('.club').append(row);
		return row;
	},
	//adds a result from search page (club-finder-search) or nearby (club-info)
	addToList: function(club){
		var row = $('#result-model').clone();
		row.removeAttr('id');
		row.find('.r-title').text(club.title);
		row.find('.r-text').html(club.description);
		row.find('.r-distance').html(Math.round(parseInt(club.distance)/100));
		ClubFinder.setOptions(row, club.options);
		
		row.removeClass('d-none');
		row.addClass('result');
		$('.results').append(row);
		return row;
	},
	//load options for a club row
	setOptions: function(row, options){
		$(options).each(function(k, v){
			var option = $('#option-model').clone();
			option.removeAttr('id');
			option.find('.o-img').attr('src', Option.getImage(v));
			option.find('.o-title').text(v);
			var option_slug = Option.getClass(v);
			option.addClass('option '+option_slug);
			option.removeClass('d-none');
			row.find('.options').append(option);
		});
	},
	//club-info contact information
	fillContact: function(club){
		$('.c-website').text(club.contact.website);
		$('.c-email').text(club.contact.email);
		$('.c-phone').text(club.contact.phone);
		
		$('.c-available').children('select').append('<option value="Sunday">Sunday: '+club.contact.available.Sunday+'</option>'
		+'<option value="Monday">Monday: '+club.contact.available.Monday+'</option>'
		+'<option value="Tuesday">Tuesday: '+club.contact.available.Tuesday+'</option>'
		+'<option value="Wednesday">Wednesday: '+club.contact.available.Wednesday+'</option>'
		+'<option value="Thursday">Thursday: '+club.contact.available.Thursday+'</option>'
		+'<option value="Friday">Friday: '+club.contact.available.Friday+'</option>'
		+'<option value="Saturday">Saturday: '+club.contact.available.Saturday+'</option>');
		$('.c-available').find('option').each(function(k, v){
			if($(v).val() == weekday()){
				$(v).attr('selected', 'selected');
				$('.c-available').find('.jcf-select-text').text($(v).text());
			}
			
		});
		$('.c-address').text(club.contact.address);
	},
	//load nearby clubs using latitude and longitude information
	fetchNearby: function(club, start_item = null, end_item = null){
		var page_number = parseInt($('.page-item.active').children('.page-link').text());
		var option = $('[name="filter_by"]').children("option:selected").val();
		var row = $('#nearby-model').clone();
		row.removeAttr('id');
		row.find('.title').text(club.title);
		row.find('.r-text').html(club.description);
		row.find('.r-distance').html(parseInt(club.distance)/100);
		ClubFinder.setOptions(row, club.options);
		row.removeClass('d-none');
		row.addClass('result');
		var lat = $('#lat').val();
		var lng = $('#lng').val();
		
		$.ajax({
			method: "GET",
			url: GET_NEARBY,
			dataType : "text",
			data: { token: ClubFinder.getRequestToken(), lat: lat, long : lng, distance_max: DISTANCE_MAX,
			limit: RESULTS_PER_PAGE, option: option, page: page_number, start_item: start_item,
		 	end_item: end_item }
		})
		.done(function( msg ) {
			var result = JSON.parse(msg);
			$('#json').html(msg);
			$('.results-pagination').removeClass('d-none');
			
			ClubFinder.refreshPagination(result.data.length, page_number);
			$('.results .result').remove();
			
			for (var i = 0; i < result.data.length; i++) {
		
				var coords = [result.data[i].attributes.clubLatitude, result.data[i].attributes.clubLongitude];
				var latLng = new google.maps.LatLng(coords[0],coords[1]);
				var club_o = new Club(result.data[i]);
				ClubFinder.addToMap(result.data[i], club_o.description);
				ClubFinder.addToList(club_o, club_o.description);				
			}
			
		}).fail(function(){
			alert('Request Failed');
		});	
		
		$('#nearby-pagination').removeClass('d-none');
		$('body').scrollTo('#results-start');
	},
	//formats information to a valid google query string
	formatGeocodeURI: function(origin_location, country, key){
		return GET_COORDINATES+origin_location+'+'+country+'&key='+key;   
	},
	//perform a search using postcode or suburb
	search: function(){
		var postcode = $('input[name="postcode"]').val();
		var suburb = $('input[name="suburb"]').val();
		
		//get coords from google. here latitude and longitude are obtained
		ClubFinder.getCoordinates(postcode, suburb);
		
		//load results from api on the list
		ClubFinder.getClubs();
		
		$('#wrap-map').removeClass('d-none');
		$('.extra-info').removeClass('d-none');
		$('body').scrollTo('#results-start');
		
	},
	//updates the pagination
	refreshPagination: function(results_length, page_number = null){
		$('#loading').removeClass('d-none');
		setTimeout(function(){
			$('#loading').addClass('d-none');
		}, 1000);
		$('.page-number').remove();
		var pages_number = Math.ceil(results_length / RESULTS_PER_PAGE);
		for(var c = 1; c <= pages_number; c++){
			var page_item = $('#pagination-model').clone();
			page_item.removeAttr('id');
			page_item.addClass('nav-page-'+c);
			page_item.children('.page-link').text(c);
			page_item.children('.page-link').attr('tabindex', c);
			if(c == page_number || (isNaN(page_number) && c == 1))
			page_item.addClass('active');
			page_item.removeClass('d-none');
			page_item.addClass('page-number');
			page_item.insertBefore('.page-next');
		}
		
		if(page_number > 1)
		$('.page-previous').removeClass('disabled');
		else
		$('.page-previous').addClass('disabled');
		if(page_number < $('.page-number').length)
		$('.page-next').removeClass('disabled');
		else
		$('.page-next').addClass('disabled');

	},
	//load results based on latitude, longitude, option filter and page 
	getClubs: function(start_item = null, end_item = null){
		var lat = $('#lat').val();
		var lng = $('#lng').val();
		var page_number = parseInt($('.page-item.active').children('.page-link').text());
		var option = $('[name="filter_by"]').children("option:selected").val();
		
		$.ajax({
			method: "GET",
			url: GET_CLUBS,
			dataType : "text",
			data: { 
				token: ClubFinder.getRequestToken(), lat: lat, long : lng, distance_max: DISTANCE_MAX,
				limit: RESULTS_PER_PAGE, option: option, page: page_number, start_item: start_item,
				end_item: end_item
		  }
		})
		.done(function( msg ) {
			var result = JSON.parse(msg);
			$('#json').html(msg);
			$('.results-pagination').removeClass('d-none');
			$('.results-number').removeClass('d-none');
			$('.r-count').text(result.data.length);
			
			ClubFinder.refreshPagination(result.data.length, page_number);
			$('.results .result').remove();
			
			for (var i = 0; i < result.data.length; i++) {
				
				var coords = [result.data[i].attributes.clubLatitude, result.data[i].attributes.clubLongitude];
				var latLng = new google.maps.LatLng(coords[0],coords[1]);
				var club_o = new Club(result.data[i]);
				ClubFinder.addToMap(result.data[i], club_o.description);
				ClubFinder.addToList(club_o, club_o.description);				
			}
			
		}).fail(function(){
			alert('Request Failed');
		});	
	},
	//here we pass postcode or suburb to google via xmlhttprequest. this is the first ajax to get center latitude and longitude data. 
	getCoordinates: function(postcode = null, suburb = null){
		if(postcode != null){
			var origin_location = postcode;
		}
		
		if(suburb != null){
			var origin_location = suburb;
		}
		
		$.ajax({
			method: "GET",
			url: ClubFinder.formatGeocodeURI(origin_location, COUNTRY_ABBR, GEO_KEY)
		})
		.done(function( msg ) {
			if(msg.results.length > 0){
				var origin_location_latitude =	msg.results[0].geometry.location.lat;
				var origin_location_longitude = msg.results[0].geometry.location.lng;
				$('#lat').val(origin_location_latitude);
				$('#lng').val(origin_location_longitude);
			} else {
				alert('location not found.');
			}			
		});	
	},
	//method called on init to get the user's position latitude and longitude	
	getLocation: function() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(ClubFinder.showPosition);
		} else {
			console.log("Geolocation is not supported by this browser.");
		}
	},
	showPosition: function(position) {
		$('#lat').val(position.coords.latitude);
		$('#lng').val(position.coords.longitude);
	}
};
//helpers
function weekday(n = null){
	var d = new Date();
	var weekday = new Array(7);
	weekday[0] = "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
	if(n != null)
	return weekday[n];
	else
	return weekday[d.getDay()];
}
$(document).ready(function(){
	$('#clear-filters').on('click', function(){
		$('[name="filter_by"]').val('');
		$('.wrap-filter .jcf-select-text').text('Select a filter');
		ClubFinder.search();
	});
	$('[name="filter_by"]').on('change', function(){
		ClubFinder.search();
	});
	$('.pagination').on('click', '.page-item .page-link', function(e){
		e.preventDefault();
		e.stopPropagation();
		var page_number = parseInt($('.page-item.active').children('.page-link').text());
		
		if($(this).parent().hasClass('page-next')){
			page_number+=1;
		}
		if($(this).parent().hasClass('page-previous')){
			page_number-=1;
		}
		if($(this).parent().hasClass('page-number')){
			page_number = parseInt($(this).text());
		}
		$('.page-item').removeClass('active');
		$('.nav-page-'+page_number).addClass('active');
		if($(this).closest('#nearby-pagination').length == 1){
			ClubFinder.fetchNearby(map);
		}		
		else{
			ClubFinder.search();
		}
    });
});
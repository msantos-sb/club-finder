<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       savagebull.com.au
 * @since      1.0.0
 *
 * @package    Club_Finder
 * @subpackage Club_Finder/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Club_Finder
 * @subpackage Club_Finder/public
 * @author     Savage Bull <msantos@savagebull.com.au>
 */
class Club_Finder_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Club_Finder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Club_Finder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( 'bootstrap-4-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', false );
		wp_enqueue_style( 'clubfinder-font-rubik', 'https://fonts.googleapis.com/css?family=Rubik', false );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/club-finder-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Club_Finder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Club_Finder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_script( 'jquery-3', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', null, $this->version, false );
		wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', null, $this->version, false );
		wp_enqueue_script( 'bootstrap-4-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', null, $this->version, false );
		wp_enqueue_script( 'jquery-1-js', 'https://code.jquery.com/jquery-1.12.4.min.js', null, $this->version, false );
		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/club-finder-public.js', null, $this->version, true );
		//wp_enqueue_script( $this->plugin_name, 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC0neLxfRyXGHRpHC0XFqGsUMNoAWHt3gM&callback=initMap', null, $this->version, true );

	}



}

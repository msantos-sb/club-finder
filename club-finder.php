<?php

/**
* The plugin bootstrap file
*
* This file is read by WordPress to generate the plugin information in the plugin
* admin area. This file also includes all of the dependencies used by the plugin,
* registers the activation and deactivation functions, and defines a function
* that starts the plugin.
*
* @link              savagebull.com.au
* @since             1.0.0
* @package           Club_Finder
*
* @wordpress-plugin
* Plugin Name:       Club Finder
* Plugin URI:        savagebull.com.au
* Description:       Find a club nearby.
* Version:           0.1
* Author:            Savage Bull
* Author URI:        savagebull.com.au
* License:           GPL-2.0+
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain:       club-finder
* Domain Path:       /languages
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
* Currently plugin version.
* Start at version 1.0.0 and use SemVer - https://semver.org
* Rename this for your plugin and update it as you release new versions.
*/
define( 'PLUGIN_NAME_VERSION', '0.1' );


// Register Custom Post Type
function clubfinder_post_type() {
	
	$labels = array(
		'name'                  => _x( 'ClubFinder', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'ClubFinder', 'text_domain' ),
		'name_admin_bar'        => __( 'Testimonial', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add Item', 'text_domain' ),
		'add_new'               => __( 'Add', 'text_domain' ),
		'new_item'              => __( 'New', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update', 'text_domain' ),
		'view_item'             => __( 'View', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not Found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' )
	);
	$args = array(
		'label'                 => __( 'Post Type', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'taxonomies'				=> [],
		'supports'              => [],
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'				=> false,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'supports' => array('title', 'page-attributes'),
		'capabilities' => [
			'create_posts' => 'do_not_allow',
		],
		'map_meta_cap' => false,
		'menu_icon' 			=> 'dashicons-groups' 
	);
	register_post_type( 'clubfinder', $args );
	add_post_type_support( 'clubfinder', 'page-attributes' );
	
}

add_action( 'init', 'clubfinder_post_type', 0 );


//enable dashicons
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}

/**
* The code that runs during plugin activation.
* This action is documented in includes/class-club-finder-activator.php
*/
function activate_club_finder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-club-finder-activator.php';
	Club_Finder_Activator::activate();
}

/**
* The code that runs during plugin deactivation.
* This action is documented in includes/class-club-finder-deactivator.php
*/
function deactivate_club_finder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-club-finder-deactivator.php';
	Club_Finder_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_club_finder' );
register_deactivation_hook( __FILE__, 'deactivate_club_finder' );

/**
* The core plugin class that is used to define internationalization,
* admin-specific hooks, and public-facing site hooks.
*/
require_once plugin_dir_path( __FILE__ ) . 'includes/class-club-finder.php';


function clubfinder_load_templates( $template ) {

	switch(get_query_var('clubfinder')){
		case "club-finder":
			return plugin_dir_path( __FILE__ ) . 'templates/club-finder-search.php';
		break;
		case "club-info":
			return plugin_dir_path( __FILE__ ) . 'templates/club-info.php';
		break;
		default:
			return $template;
		break;
	}

	
}

add_filter( 'template_include', 'clubfinder_load_templates', 99 );


/**
* Begins execution of the plugin.
*
* Since everything within the plugin is registered via hooks,
* then kicking off the plugin from this point in the file does
* not affect the page life cycle.
*
* @since    1.0.0
*/
function run_club_finder() {
	$plugin = new Club_Finder();
	$plugin->run();
}
run_club_finder();

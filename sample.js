{
    "data": [
        {
            "type": "clubSearch",
            "id": "CLUB_1",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_1",
                "clubName": "Mornington",
                "clubLatitude": "58.002637",
                "clubLongitude": "-36.349405",
                "clubState": "Tasmania",
                "distance": "16466.76635098319"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_1/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_1"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_1"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_1"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_1/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_1"
            }
        },
        {
            "type": "clubSearch",
            "id": "CLUB_10",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_10",
                "clubName": "Traralgon RSL",
                "clubLatitude": "53.250638",
                "clubLongitude": "-99.693583",
                "clubState": "Victoria",
                "distance": "15314.891740658808"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_10/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_10"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_10"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_4"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_5"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_6"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_10/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_10"
            }
        },
        {
            "type": "clubSearch",
            "id": "CLUB_11",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_11",
                "clubName": "Moe",
                "clubLatitude": "30.690280",
                "clubLongitude": "-163.108769",
                "clubState": "New South Wales",
                "distance": "12802.818483353629"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_11/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_11"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_11"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_7"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_8"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_9"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_11/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_11"
            }
        },
        {
            "type": "clubSearch",
            "id": "CLUB_12",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_12",
                "clubName": "Phillip Island",
                "clubLatitude": "32.298354",
                "clubLongitude": "-44.784670",
                "clubState": "Australian Capital Territory",
                "distance": "13542.727933444583"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_12/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_12"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_12"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_10"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_11"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_12/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_12"
            }
        },
        {
            "type": "clubSearch",
            "id": "CLUB_13",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_13",
                "clubName": "Drouin",
                "clubLatitude": "50.284930",
                "clubLongitude": "58.754901",
                "clubState": "Tasmania",
                "distance": "16296.24362258883"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_13/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_13"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_13"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_13"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_14"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_15"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_13/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_13"
            }
        },
        {
            "type": "clubSearch",
            "id": "CLUB_14",
            "contact": [{ "website": "www", "email": "msantos@savagebull.com.au", "phone" : "+5531994098814", "available": ["Sunday": "From 9am Until 10pm", "Monday": "From 9am Until 8pm", "Tuesday": "From 9am Until 10pm", "Wednesday": "From 9am Until 10pm", "Thursday": "From 9am Until 10pm", "Friday": "From 9am Until 10pm", "Saturday": "From 9am Until 10pm" ], "address" : "21 Sandbelt Street, Sandringham, 3956" }], 
            "options": ["Function Bookings", "Jack Attack" , "Social Bowls", "Meals", "Entertainment", "Tab"],
            "attributes": {
                "clubId": "CLUB_14",
                "clubName": "Gladstone Park",
                "clubLatitude": "-25.957752",
                "clubLongitude": "-76.907841",
                "clubState": "Queensland",
                "distance": "6730.308477512648"
            },
            "relationships": {
                "metadata": {
                    "data": {
                        "type": "metadataBag",
                        "id": "a78e6909-d987-44ea-abf8-9cdbf4ecdc17"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_14/metadata"
                    }
                },
                "club": {
                    "data": {
                        "type": "club",
                        "id": "CLUB_14"
                    },
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_14"
                    }
                },
                "clubCategories": {
                    "data": [
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_16"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_17"
                        },
                        {
                            "type": "clubCategory",
                            "id": "CLUB_CATEGORIES_18"
                        }
                    ],
                    "links": {
                        "related": "http://localhost:8000/api/clubs/CLUB_14/categories"
                    }
                }
            },
            "links": {
                "self": "http://localhost:8000/search-clubs?clubId=CLUB_14"
            }
        }
    ]
}
=== Club Finder ===
Contributors: Matteus Barbosa
Donate link: savagebull.com.au
Tags: clubs, search, maps
Requires at least: 5
Tested up to: 5
Stable tag: 5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Find a club nearby.

== Description ==

Find a club nearby.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `club-finder.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Access /club-finder through your browser
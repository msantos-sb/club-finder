module.exports = {
    sassDir:          'public/css/sass/',
    sassMainFileName: 'public/css/club-finder-public.css',
    cssDir:           'public/css/',
    cssMainFileDir:   'public/css/',
    cssMainFileName:  'club-finder-public',
    jsDir:            'public/js/',
    imgDir:           'public/images/',
    imgSourceDir:     'public/sourceimages/',

    // sftp server
    sftpServer:       'example.com',
    sftpPort:         '2121',
    sftpLogin:        'login',
    sftpPas:          'password',
    sftpDestination:  '/pathTo/css'
  };
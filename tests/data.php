<?php $data = [
    'items' => [
        [ 
            'title' => 'Lorem Ipsum is simply dummy text of', 'distance' => 5, 'link' => '/?clubfinder=club-info&club_id=14', 'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,',
            'options' => [
                ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                ]
            ],
            [ 
                'title' => 'Lorem Ipsum is simply dummy text of', 'distance' => 5, 'link' => '/?clubfinder=club-info&club_id=14', 'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,',
                'options' => [
                    ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                    ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                    ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                    ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                    ]
                ],
                [ 
                    'title' => 'Lorem Ipsum is simply dummy text of', 'distance' => 5, 'link' => '/?clubfinder=club-info&club_id=14', 'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,',
                    'options' => [
                        ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                        ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                        ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                        ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                        ]
                    ],
                    [ 
                        'title' => 'Lorem Ipsum is simply dummy text of', 'distance' => 5, 'link' => '/?clubfinder=club-info&club_id=14', 'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,',
                        'options' => [
                            ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                            ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                            ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                            ['title' => 'Lorem Ipsum', 'img' => plugin_dir_url( __FILE__ ) . '../public/img/item.png', 'link' => '/?clubfinder=club-info&club_id=14' ],
                            ]
                            ]
                            ]
                        ];

                        $club['website'] = 'www.sandybowls.bows.com.au';
                        $club['email'] = 'email@sandybowls.bows.com.au';
                        $club['phone'] = '(03) 1234-5678';
                        $club['available_status'] = ' Open today – Until 9pm';
                        $club['address'] = '21 Sandbelt Street, Sandringham, 3956';
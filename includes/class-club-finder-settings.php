<?php

class ClubFinderSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Club Finder Settings', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'clubfinder_option' );
        ?>
        <div class="wrap">
            <h1>Setup basic connection information for the Club Finder Plugin (by Savage Bull)</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'club_finder_group' );
                do_settings_sections( 'my-setting-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'club_finder_group', // Option group
            'clubfinder_option', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'clubfinder_default_section', // ID
            'Club Finder Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  

        add_settings_field(
            'clubs_api', // ID
            'Clubs API route', // Title 
            array( $this, 'clubs_api_callback' ), // Callback
            'my-setting-admin', // Page
            'clubfinder_default_section' // Section           
        );   
        
        add_settings_field(
            'clubs_nearby_api', // ID
            'Clubs nearby/related API route', // Title 
            array( $this, 'clubs_nearby_api_callback' ), // Callback
            'my-setting-admin', // Page
            'clubfinder_default_section' // Section           
        );    

        add_settings_field(
            'Google Geocode Key', // ID
            'Geocode Key from Google', // Title 
            array( $this, 'geo_key_callback' ), // Callback
            'my-setting-admin', // Page
            'clubfinder_default_section' // Section           
        );  

        add_settings_field(
            'Google Maps Key', // ID
            'Maps Key from Google', // Title 
            array( $this, 'maps_key_callback' ), // Callback
            'my-setting-admin', // Page
            'clubfinder_default_section' // Section           
        );  
        
        add_settings_field(
            'Results per page', // ID
            'Numbers of results to show per page', // Title 
            array( $this, 'results_per_page_callback' ), // Callback
            'my-setting-admin', // Page
            'clubfinder_default_section' // Section           
        ); 

      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        if( isset( $input['clubs_api'] ) )
        $new_input['clubs_api'] = sanitize_text_field( $input['clubs_api'] );

        if( isset( $input['clubs_nearby_api'] ) )
        $new_input['clubs_nearby_api'] = sanitize_text_field( $input['clubs_nearby_api'] );

        if( isset( $input['geo_key'] ) )
        $new_input['geo_key'] = sanitize_text_field( $input['geo_key'] );

        if( isset( $input['maps_key'] ) )
        $new_input['maps_key'] = sanitize_text_field( $input['maps_key'] );

        if( isset( $input['results_per_page'] ) )
        $new_input['results_per_page'] = sanitize_text_field( $input['results_per_page'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print '<strong>Important information</strong>:'
        .'<ul>'
        .'<li>Clubs API route must accept parameter names ["club_id", "lat", "long", "distance_max", "limit", "page", "option"  ] </li>'
        .'<li>Nearby clubs API route must accept parameter names ["club_id", "lat", "long", "distance_max", "limit", "page", "start_item", "option" ] </li>'
        .'</ul>';
    }

        /** 
     * Get the settings option array and print one of its values
     */
    public function clubs_nearby_api_callback()
    {
        printf(
            '<input type="text" id="clubs_nearby_api" name="clubfinder_option[clubs_nearby_api]" value="%s" />',
            isset( $this->options['clubs_nearby_api'] ) ? esc_attr( $this->options['clubs_nearby_api']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function clubs_api_callback()
    {
        printf(
            '<input type="text" id="clubs_api" name="clubfinder_option[clubs_api]" value="%s" />',
            isset( $this->options['clubs_api'] ) ? esc_attr( $this->options['clubs_api']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
     public function geo_key_callback()
     {
         printf(
             '<input type="text" id="geo_key" name="clubfinder_option[geo_key]" value="%s" />',
             isset( $this->options['geo_key'] ) ? esc_attr( $this->options['geo_key']) : ''
         );
     }

     /** 
     * Get the settings option array and print one of its values
     */
    public function maps_key_callback()
    {
        printf(
            '<input type="text" id="maps_key" name="clubfinder_option[maps_key]" value="%s" />',
            isset( $this->options['maps_key'] ) ? esc_attr( $this->options['maps_key']) : ''
        );
    }

      /** 
     * Get the settings option array and print one of its values
     */
    public function results_per_page_callback()
    {
        printf(
            '<input type="text" id="results_per_page" name="clubfinder_option[results_per_page]" value="%s" />',
            isset( $this->options['results_per_page'] ) ? esc_attr( $this->options['results_per_page']) : ''
        );
    }
}

if( is_admin() )
    $my_settings_page = new ClubFinderSettingsPage();
<?php

/**
 * Fired during plugin activation
 *
 * @link       savagebull.com.au
 * @since      1.0.0
 *
 * @package    Club_Finder
 * @subpackage Club_Finder/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Club_Finder
 * @subpackage Club_Finder/includes
 * @author     Savage Bull <msantos@savagebull.com.au>
 */
class Club_Finder_Activator {

	public static function add_search_page(){

		$html = file_get_contents(__DIR__ . '/../public/partials/forms/search.html');
		$html .= file_get_contents(__DIR__ . '/../public/partials/forms/map.html');
		
		$my_post = array(
			'post_title'    => wp_strip_all_tags( 'Club Finder' ),
			'post_content'  => $html,
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_type'     => 'clubfinder',
		);

		// Insert the post into the database
		$id = wp_insert_post( $my_post );
	
		return $id;
	}

	public static function add_club_page(){

		$html = file_get_contents(__DIR__ . '/../public/partials/forms/search.html');
		$html .= file_get_contents(__DIR__ . '/../public/partials/forms/map.html');
		
		$my_post = array(
			'post_title'    => wp_strip_all_tags( 'Club Info' ),
			'post_content'  => $html,
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_type'     => 'clubfinder',
		);

		// Insert the post into the database
		$id = wp_insert_post( $my_post );
	
		return $id;
	}



	public static function remove_posts(){
		$allposts= get_posts( array('post_type'=>'clubfinder','numberposts'=>-1) );
		foreach ($allposts as $eachpost) {
			wp_delete_post( $eachpost->ID, true );
		}
	}


	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		self::remove_posts();
		self::add_search_page();
		self::add_club_page();
	}

}

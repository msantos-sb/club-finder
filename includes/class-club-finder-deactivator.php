<?php

/**
 * Fired during plugin deactivation
 *
 * @link       savagebull.com.au
 * @since      1.0.0
 *
 * @package    Club_Finder
 * @subpackage Club_Finder/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Club_Finder
 * @subpackage Club_Finder/includes
 * @author     Savage Bull <msantos@savagebull.com.au>
 */
class Club_Finder_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
